# Sandbox for Intel Technical Writing School

This sandbox will help you understand the basics of Git workflow and the development process for open-source documentation.

## Prerequisites

- GitLab account

## Repository Contents

This repository contains the sources for the [website hosted on GitLab Pages](https://outoftardis.gitlab.io/tech-writing-school/).

- ``/docs``: Sphinx project with the source files written in reStructuredText
  -  ``/source``: the sources for the website (in ``.rst`` format) and a configuration file (``conf.py``) for the Sphinx project
  - ``Makefile`` and ``make.bat``: make files to build the docs
- ``.gitignore``: a file that specifies intentionally untracked files that Git should ignore
- ``.gitlab-ci.yml``: CI/CD scripts ([Continuous Methodologies](https://outoftardis.gitlab.io/tech-writing-school/ci-process.html))
- ``README.md``: this file that you are reading; provides basic information about the project

## Workflow

If you want to contribute to the project, follow this workflow:

- Fork the repository.
- Create a new branch.
- Introduce a minor change ([contribution ideas](https://outoftardis.gitlab.io/tech-writing-school/sandbox/index.html)).
- Create a merge request.
- Wait for it to be reviewed and approved.
- Check that the change is reflected on the website.

Refer to [instructions](https://outoftardis.gitlab.io/tech-writing-school/contribution.html) for these steps.

## GitLab CI

This project's static Pages are built by GitLab CI, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

