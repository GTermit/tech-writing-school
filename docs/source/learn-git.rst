.. _learn_git:

Learning Git
============

Git is a version control system that is used for collaborative software development.

Resources
*********

- `Learn Git at Codecadamy <https://www.codecademy.com/learn/learn-git>`_
- `Learn Git Branching (interactive sandbox) <https://learngitbranching.js.org/>`_
- `Git Cheat Sheet <https://training.github.com/downloads/github-git-cheat-sheet/>`_