.. Working in Open Source documentation master file, created by
   sphinx-quickstart on Thu Jan 21 18:30:41 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Working in Open Source
======================

Intel Technical Writing school created a `sandbox repository <https://gitlab.com/outoftardis/tech-writing-school>`_
for you to learn how collaboration on an open-source documentation works. This website is created from the source files
in that repository, and it offers you a quick introduction into some of the concepts and tools used in open source.

You will find:

- overview of open source concepts
- links to other resources with more in-detail instructions and explanations
- explanation of how the automated publishing pipeline works for GitLab Pages
- instructions on how to make you first contribution in an open-source project

.. toctree::
   :maxdepth: 2
   :caption: Tools and Workflow

   learn-git.rst
   contribution.rst
   work-with-sphinx.rst
   use-rst.rst
   ci-process.rst

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Sandbox

   sandbox/*