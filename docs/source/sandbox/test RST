..  It is a test file.
    Because I want to try reStructuredText

Mother Goose Rhymes
===================
.. contents:: Content
   :depth: 3

About
-----
Information in this chapter is taken from the article `Mother Goose Rhymes <https://www.pookpress.co.uk/project/mother-goose-rhymes-history/>`_.

The figure of Mother Goose is an imaginary author of a collection of fairy tales and nursery rhymes — often published as «Mother Goose Rhymes». As a character, she only appears in one nursery rhyme.

«Mother Goose» is generally depicted in illustrations as an elderly countrywoman in a tall hat and shawl (a costume identical to the peasant costume worn in Wales in the early twentieth century). But she is also sometimes depicted as a goose (usually wearing a bonnet). She is credited with the Mother Goose stories and rhymes popularized in the 1700s in English-language literature, although no specific writer has ever been identified with such a name.

Some Rhymes
-----------
Information in this chapter is taken from the article `The Real Mother Goose <http://www.cybercrayon.net/readingroom/books/mothergoose/realmothergoose.html>`_.

Here there are following poems:

* `Pat-a-cake`_
* `Robin Redbreast`_
* `Thirty days hath September`_

PAT-A-CAKE
~~~~~~~~~~
| Pat-a-cake, pat-a-cake,
| Baker's man!
| So I do, master,
| As fast as I can.

| Pat it, and prick it,
| And mark it with **T**,
| Put it in the oven
| For *Tommy* and me.

ROBIN REDBREAST
~~~~~~~~~~~~~~~
| Little *Robin Redbreast* sat upon a tree,
| Up went Pussy-Cat, down went he,
| Down came Pussy-Cat, away Robin ran,
| Says little *Robin Redbreast*: "Catch me if you can!"

| Little *Robin Redbreast* jumped upon a spade,
| *Pussy-Cat* jumped after him, and then he was afraid.
| Little *Robin* chirped and sang, and what did Pussy say?
| Pussy-Cat said: "Mew, mew, mew," and *Robin* flew away.


THIRTY DAYS HATH SEPTEMBER
~~~~~~~~~~~~~~~~~~~~~~~~~~
| Thirty days hath September,
| April, June, and November;
| February has twenty-eight alone,
| All the rest have thirty-one,
| Excepting leap-year, that's the time
| When February's days are twenty-nine.


Statistics
----------
Poems are quite short — see the `Table 1`_.

.. table::  _`Table 1`. Length of poems

  +----------------------------+-------------------+   
  | Poem                       | Number of string  |
  +============================+===================+
  | Pat-a-cake                 | 8                 |
  +----------------------------+-------------------+
  | Robin Redbreast            | 8                 |
  +----------------------------+-------------------+
  | Thirty days hath September | 6                 |
  +----------------------------+-------------------+

