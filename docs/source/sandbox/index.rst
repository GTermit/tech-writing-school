Make you first contribution
===========================

You are free to make any contribution you want, but here is a suggestion of what you might want to do:

#. Create your copy of `tech-writing-school <https://gitlab.com/outoftardis/tech-writing-school>`_ by forking the project.
#. Add a new reStructuredText file within the ``docs/source/sandbox`` folder. Give it a meaningful name.
#. Use the existing files in the repository as a reference regarding the reStructuredText syntax.
   
   .. note:: Adding just a title is enough.

#. Commit your changes.
#. Create a merge request.
#. Wait for a review and approval.

.. note::

   Follow the instructions about :ref:`contribute_to_open_source`.