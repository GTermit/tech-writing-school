.. _use_rst:

Using reStructuredText
======================

reStructuredText (RST, reST) is plaintext markup syntax that is used to create documentation with :ref:`Sphinx <work_with_sphinx>` doc generator.

Resources
*********

- `RST Cheatsheet by Roberto Alsina on GitHub <https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst>`_
- `RST & Sphinx Cheatsheet (TYPO3 Documentation) <https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CheatSheet.html>`_
- `Common Pitfalls (TYPO3 Documentation) <https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/CommonPitfalls/Index.html#rest-common-pitfalls>`_
- `RST & Sphinx Cheatsheet (OpenAlea Document Center) <http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html>`_
- `Quick reStructuredText Reference (docutils) <https://docutils.sourceforge.io/docs/user/rst/quickref.html>`_
